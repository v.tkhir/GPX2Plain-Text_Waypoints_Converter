"""
Copyright (c) 2015 Vasyl Tkhir (vasyl.tkhir@gmail.com)


Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# -*- coding: utf-8 -*-
import sys
import json
import httplib
from xml.etree import ElementTree
import math
import time
from sys import stdout, stderr
import argparse

#uncomment language(s) to add it (them) to options
lang_names = {
#	"ar"	:	"Arabic",
#	"bg"	:	"Bulgarian",
#	"bn"	:	"Bengali",
#	"ca"	:	"Catalan",
#	"cs"	:	"Czech",
#	"da"	:	"Danish",
#	"de"	:	"German",
#	"el"	:	"Greek",
	"en"	:	"English",
#	"en-AU"	:	"English (Australian)",
#	"en-GB"	:	"English (Great Britain)",
#	"es"	:	"Spanish",
#	"eu"	:	"Basque",
#	"fa"	:	"Farsi",
#	"fi"	:	"Finnish",
#	"fil"	:	"Filipino",
#	"fr"	:	"French",
#	"gl"	:	"Galician",
#	"gu"	:	"Gujarati",
#	"hi"	:	"Hindi",
#	"hr"	:	"Croatian",
#	"hu"	:	"Hungarian",
#	"id"	:	"Indonesian",
#	"it"	:	"Italian",
#	"iw"	:	"Hebrew",
#	"ja"	:	"Japanese",
#	"kn"	:	"Kannada",
#	"ko"	:	"Korean",
#	"lt"	:	"Lithuanian",
#	"lv"	:	"Latvian",
#	"ml"	:	"Malayalam",
#	"mr"	:	"Marathi",
#	"nl"	:	"Dutch",
#	"no"	:	"Norwegian",
	"pl"	:	"Polish",
#	"pt"	:	"Portuguese",
#	"pt-BR"	:	"Portuguese (Brazil)",
#	"pt-PT"	:	"Portuguese (Portugal)",
#	"ro"	:	"Romanian",
	"ru"	:	"Russian",
#	"sk"	:	"Slovak",
#	"sl"	:	"Slovenian",
#	"sr"	:	"Serbian",
#	"sv"	:	"Swedish",
#	"ta"	:	"Tamil",
#	"te"	:	"Telugu",
#	"th"	:	"Thai",
#	"tl"	:	"Tagalog",
#	"tr"	:	"Turkish",
	"uk"	:	"Ukrainian",
#	"vi"	:	"Vietnamese",
#	"zh-CN"	:	"Chinese (Simplified)",
#	"zh-TW"	:	"Chinese (Traditional)"
}

class Point:
	lat = float(0)
	lon = float(0)
	def __init__(self, lat, lon):
		self.lat = float(lat)
		self.lon = float(lon)
 
	def distance_on_unit_sphere(self, other):
		# Convert latitude and longitude to 
		# spherical coordinates in radians.
		degrees_to_radians = math.pi/180.0
			 
		# phi = 90 - latitude
		phi1 = (90.0 - self.lat)*degrees_to_radians
		phi2 = (90.0 - other.lat)*degrees_to_radians
			 
		# theta = longitude
		theta1 = self.lon*degrees_to_radians
		theta2 = other.lon*degrees_to_radians
			 
		# Compute spherical distance from spherical coordinates.
			 
		# For two locations in spherical coordinates 
		# (1, theta, phi) and (1, theta', phi')
		# cosine( arc length ) = 
		#    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
		# distance = rho * arc length
		 
		cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) + 
			   math.cos(phi1)*math.cos(phi2))
		arc = math.acos( cos )
	 
		# Remember to multiply arc by the radius of the earth 
		# in your favorite set of units to get length.
		radius_of_earth_km = 6371
		return arc * radius_of_earth_km
	
class City:
	Name = ""
	#TBD use this values to mone out of the city to reduce requests
	northeast = Point(0,0)
	southwest = Point(0,0)
	
	def __init__(self, name, northeast, southwest):
		self.Name = name
		self.northeast = northeast
		self.southwest = southwest

class Error:
	InternetConnectionError = 1
	ZeroResult = 2
	OverQueryLimit = 3
	RequestDenied = 4
	InvalidRequest = 5
	AddressNotFound = 6
	UnknownError = 7
	UdefinedError = 8
	
class WayPoint:
	Name = ""
	Dist = 0
	def __init__(self, name, distance):
		self.Name = name
		self.Dist = distance
	
	def __repr__(self):
		return "{1}\t{0}".format(self.Name.encode('utf8'), int(self.Dist))

def get_city_name(lat, lon, lang):
	"""
	There is Geolocating Google API for python, but my goal is to have all functionality without third-party 
	libraries. 
	"""
	try:
		conn = httplib.HTTPSConnection("maps.googleapis.com")
		conn.request("GET", "/maps/api/geocode/json?latlng={0},{1}&sensor=true&language={2}".format(lat, lon, lang))
		response = conn.getresponse()
	except:
		return Error.InternetConnectionError
		
	if not response.status == 200:
		return Error.InternetConnectionError
	data = response.read()

	conn.close()
	jdata = json.loads(data)
	if not jdata["status"] == "OK":
		if jdata["status"] == "ZERO_RESULTS":
			return Error.ZeroResult
		elif jdata["status"] == "OVER_QUERY_LIMIT":
			return Error.OverQueryLimit
		elif jdata["status"] == "REQUEST_DENIED":
			return Error.RequestDenied
		elif jdata["status"] == "INVALID_REQUEST":
			return Error.InvalidRequest
		elif jdata["status"] == "UNKNOWN_ERROR":
			return Error.UnknownError
		else:
			return Error.UdefinedError

	for result in jdata["results"]:
		if "locality" in result["types"]:
			northeast = Point(result["geometry"]["bounds"]["northeast"]["lat"], result["geometry"]["bounds"]["northeast"]["lng"])
			southwest = Point(result["geometry"]["bounds"]["southwest"]["lat"], result["geometry"]["bounds"]["southwest"]["lng"])

			for address_component in result["address_components"]:
				if "locality" in address_component["types"]:
					found_address = True
					#at last we can get the city
					return City(address_component["long_name"], northeast, southwest)
					
	return Error.AddressNotFound

def parse_args():
	parser = argparse.ArgumentParser(description="""
This program allows to generate Plain-Text Waypoints (city names and POIs) from GPX track, downloaded from http://www.gpsies.com
Please download files as "Track & Waypoint".
""")

	parser.add_argument("-i", "--input", help='input gpx file name', required=True)
	parser.add_argument("-o", "--output", help='output text file name', required=True)
	parser.add_argument("-l", "--language", help='language of output', type=str, default="en", choices=(lang_names.keys()))
	parser.add_argument("-d", "--distance", help='distance between points to check', type=float, default=1.0)
	parser.add_argument("-r", "--wptremoteness", help='max distance from track to waypoint to include', type=float, default=2.0)
	parser.add_argument("-e", "--excludewpts", help='exclude POIs', default=False, action="store_true")
	parser.add_argument("-c", "--cityremoteness", help='min distance between cities to include', type=float, default=0.0)
	parser.add_argument("-v", "--version", action='version', version='Plain-Text Waypoints Generator v0.1')
	return parser.parse_args()
	
def main():
	args = parse_args()
		
	tree = ElementTree.ElementTree()
	tree.parse(args.input)
	root = tree.getroot()

	
	waypoints = {}
	if not args.excludewpts:
		for wpt in root.findall("{http://www.topografix.com/GPX/1/1}wpt"):
			waypoints[Point(wpt.attrib["lat"], wpt.attrib["lon"])] = wpt.find("{http://www.topografix.com/GPX/1/1}name").text
		
	trk = root.find("{http://www.topografix.com/GPX/1/1}trk")
	
	manual = []
	distance = 0
	total_distance = 0
	last_checked_distance = 0
	last_point = None
	last_city_added = None
	max_attempts = 3
	current_attempt = 1
	last_added_waypoint = None
	
	
	for trkpt in trk.iter("{http://www.topografix.com/GPX/1/1}trkpt"):
		if last_point == None:
			last_point = Point(trkpt.attrib["lat"], trkpt.attrib["lon"])
			continue
		else:
			point = Point(trkpt.attrib["lat"], trkpt.attrib["lon"])
			total_distance += last_point.distance_on_unit_sphere(point)
			last_point = point
		
	last_point = None
	
	for trkpt in trk.iter("{http://www.topografix.com/GPX/1/1}trkpt"):
		if last_point == None:
			last_point = Point(trkpt.attrib["lat"], trkpt.attrib["lon"])
			continue
		else:
			point = Point(trkpt.attrib["lat"], trkpt.attrib["lon"])
			distance += last_point.distance_on_unit_sphere(point)
		
			stdout.write("\rLoading data {0}% ".format(int(distance/total_distance*100)))
			stdout.flush()
			#add waypoints to the manual
			#unfortunately they could be unordered
			for waypoint in waypoints.keys():
				if waypoint.distance_on_unit_sphere(point) < args.wptremoteness\
					and waypoint.distance_on_unit_sphere(point) > waypoint.distance_on_unit_sphere(last_point):
					manual.append(WayPoint(waypoints[waypoint], distance))
					del waypoints[waypoint]
					
			last_point = point
			
			if  distance - last_checked_distance > args.distance:
				while current_attempt <= max_attempts:
					last_checked_distance = distance
					#try to get result
					result = get_city_name(point.lat,point.lon, args.language)
					
					#success
					if isinstance(result, City):
						current_attempt = 1
						city = result
						if not last_city_added == city.Name:
							waypoint = WayPoint(city.Name, distance)
							if last_added_waypoint == None or waypoint.Dist - last_added_waypoint.Dist > args.cityremoteness:
								manual.append(waypoint)
								last_city_added = city.Name
								last_added_waypoint = waypoint
							
						#exit while loop
						break
					#fail
					elif isinstance(result, int):
						if result == Error.InternetConnectionError:
							stderr.write("Internet connection error.\n")
							if current_attempt >= max_attempts:
								stderr.write("Exiting.\n")
								sys.exit(1)
							else:
								current_attempt += 1
								stderr.write("Trying again. Attempt {0} of {1}\n".format(current_attempt, max_attempts))
						elif result == Error.ZeroResult:
							#just move to next point
							
							#exit while loop
							break
						elif result == Error.OverQueryLimit:
							#Oh my google. Are you tired?
							stderr.write("You have used your quota. Waiting 60 seconds.\n")
							time.sleep(60)
						elif result == Error.RequestDenied:
							stderr.write("The request is not valid. Please check the request string and try again (possible error in API key).\n")
							sys.exit(1)
						elif result == Error.InvalidRequest:
							stderr.write("The request is not valid. Please check the request string and try again (possible error in lat, lon, missing data etc.).\n")
							sys.exit(1)
						elif result == Error.AddressNotFound:
							#Address not found. It could be just a road.
							
							#exit while loop
							break
						elif result == Error.UnknownError:
							stderr.write("Unknown error occupied.\n")
							if current_attempt > max_attempts:
								stderr.write("Exiting.\n")
							else:
								current_attempt += 1
								stderr.write("Trying again. Attempt {0} of {1}\n".format(current_attempt, max_attempts))
						else:
							stderr.write("Unexpected error occupied. Continue.\n")
					else:
						stderr.write("Unexpected data received: {0}.\nExiting.\n".format(result))
						sys.exit(1)
	print
	
	manual.append(WayPoint("Finish".decode('utf-8'), distance))
	
	text_file = open(args.output, "w")
	for waypoint in manual:
		text_file.write("{0}\n".format(waypoint))
	text_file.close()

	print "Travel waypoints successfully generated!\nOpen {0} to see them.".format(args.output)
	
	return 0
	
if __name__ == "__main__":
    main()